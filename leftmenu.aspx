<%@ Page EnableSessionState="false" Language="vb" AutoEventWireup="false" Codebehind="leftmenu.aspx.vb" Inherits="mediesprak.leftmenu"%>
<%@ Outputcache Duration="900" Location="Any" VaryByParam="*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>NTB - Norsk Telegrambyr�</title>
		<link rel="stylesheet" href="css/styles.css">
	</HEAD>
	<body bgcolor="#d1d5e3" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table width="150" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
			<tr>
				<td><img src="images/t.gif" height="4" width="1"><br>
				</td>
			</tr>
		</table>
		<table width="150" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="images/t.gif" height="1" width="12"></td>
				<td valign="top" class="leftmenu_heading">
					<img src="images/t.gif" height="10" width="1"><br>
					<font color="red">Mediespr�k</font> anbefaler
				</td>
			</tr> 
			<tr>
				<td><img src="images/t.gif" height="1" width="12"></td>
				<td valign="top" class="leftmenu">
					<img src="images/t.gif" height="3" width="1"><br>
					Siste&nbsp;20
				</td>
			</tr>
			<tr>
				<td><img src="images/t.gif" height="1" width="12"></td>
				<td valign="top" class="leftmenu_heading">
					<img src="images/t.gif" height="10" width="1"><br>
					<form name="search" method="get" action="/forum/search.asp" target="_top">
						<input type="hidden" name="SM" value="3"> <input type="hidden" name="SI" value="TXT">
						<input type="hidden" name="FM" value="2"> <input type="hidden" name="OB" value="1">
						<input type="text" name="KW" size="12" class="artikkel_tekst"> <input type="submit" value="S�k" class="artikkel_tekst">
					</form>
				</td>
			</tr>
			<tr>
				<td><img src="images/t.gif" height="1" width="12"></td>
				<td valign="top" class="leftmenu">
					<img src="images/t.gif" height="3" width="1"><br>
					<%= list %>
				</td>
			</tr>
			<tr>
				<td><img src="images/t.gif" height=1 width=12></td>
				<td valign=top class="leftmenu_heading">
					<img src="images/t.gif" height=20 width=1><br>
					<a href="section.aspx?section=ANBEFALINGER" target="main">Flere&nbsp;anbefalinger</a>
				</td>
			</tr>
		</table>
	</body>
</HTML>

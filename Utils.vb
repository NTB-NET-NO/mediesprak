Imports System.Data.SqlClient
Imports System.Web.Mail
Imports mediesprak.Global
Imports System.IO

Module Utils
    Public phonebookSort As String

    Public Function CheckLogin(ByVal userid, ByVal password, ByVal maingroup, ByVal ip) As String
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand("_CheckUserLogin", dbNTB)
        Dim rows As Integer
        Dim maingroups As String

        cd.CommandType = CommandType.StoredProcedure
        cd.Parameters.Add(New SqlParameter("@userid", SqlDbType.VarChar, 20))
        cd.Parameters.Item("@userid").Value = userid
        cd.Parameters.Add(New SqlParameter("@password", SqlDbType.VarChar, 20))
        cd.Parameters.Item("@password").Value = password
        cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.Int))
        cd.Parameters.Item("@maingroup").Value = CInt(maingroup)
        dbNTB.Open()
        CheckLogin = cd.ExecuteScalar
        dbNTB.Close()

        If CheckLogin <> "" And userid <> CheckIP(ip, maingroup, userid) Then
            CheckLogin = ""
        End If

    End Function

    Public Function FormatIP(ByVal ip As String) As String
        Dim s As String
        Dim j, i, oldi As Integer
        s = ip
        i = 0
        oldi = 0
        FormatIP = ""
        For j = 1 To 4
            i = s.IndexOf(".", i + 1)
            If i = -1 Then i = s.Length
            If FormatIP = "" Then
                FormatIP += CInt(s.Substring(oldi, i - oldi)).ToString("d3")
            Else
                FormatIP += "." + CInt(s.Substring(oldi, i - oldi)).ToString("d3")
            End If
            oldi = i + 1
        Next
    End Function

    Public Function CheckIPRange(ByVal iprange As String, ByVal ip As String) As Boolean
        Dim arr As Array
        Dim i As Integer
        Dim ipf, ipt As String

        CheckIPRange = False
        arr = Split(iprange, ",")
        For i = 0 To arr.Length - 1
            If arr(i).IndexOf("-") = -1 Then
                CheckIPRange = (arr(i) = ip)
            Else
                ipf = CType(arr(i), String).Substring(0, CType(arr(i), String).IndexOf("-"))
                ipt = CType(arr(i), String).Substring(CType(arr(i), String).IndexOf("-") + 1, CType(arr(i), String).Length - CType(arr(i), String).IndexOf("-") - 1)
                CheckIPRange = (FormatIP(ip) >= FormatIP(ipf)) And (FormatIP(ip) <= FormatIP(ipt))
            End If

            If CheckIPRange Then Exit For
        Next
    End Function

    Public Function CheckIP(ByVal ip As String, ByVal maingroup As String, Optional ByVal userid As String = "")
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand("_CheckIPRange", dbNTB)
        Dim dr As SqlDataReader
        Dim rows As Integer

        dbNTB.Open()
        cd.CommandType = CommandType.StoredProcedure
        cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.VarChar, 12))
        cd.Parameters.Item("@maingroup").Value = maingroup

        If userid <> "" Then
            cd.Parameters.Add(New SqlParameter("@userid", SqlDbType.VarChar, 50))
            cd.Parameters.Item("@userid").Value = userid
        End If

        dr = cd.ExecuteReader
        CheckIP = ""

        If Not dr.HasRows() Then
            CheckIP = userid
        Else
            While dr.Read
                If CheckIPRange(dr("IPRange"), ip) Then
                    CheckIP = dr("UserId")
                    If CheckIP = userid Then Exit While
                End If
            End While
        End If

        dr.Close()

        If CheckIP <> "" Then
            cd.CommandType = CommandType.Text
            cd.CommandText = "update USERS set LastLoggedIn = getdate() where UserId = '" & CheckIP & "'"
            rows = cd.ExecuteNonQuery()
        End If
        dbNTB.Close()
    End Function

    Public Function SendEmail(ByVal From, ByVal ToAdd, ByVal Subject, ByVal Body)
        Dim Email As SmtpMail
        Email.SmtpServer = ConfigurationSettings.AppSettings("mailserver")
        Email.Send(From, ToAdd, Subject, Body)
        SendEmail = True
    End Function

    Public Function SaveDLStats(ByVal userid, ByVal articleid, ByVal maingroup, ByVal text, ByVal picture)
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand("_SaveDLStats", dbNTB)
        cd.CommandType = CommandType.StoredProcedure

        dbNTB.Open()
        cd.Parameters.Add(New SqlParameter("@userid", SqlDbType.VarChar, 20))
        cd.Parameters.Item("@userid").Value = userid
        cd.Parameters.Add(New SqlParameter("@articleid", SqlDbType.Int))
        cd.Parameters.Item("@articleid").Value = articleid
        cd.Parameters.Add(New SqlParameter("@dldate", SqlDbType.VarChar, 8))
        cd.Parameters.Item("@dldate").Value = Format(Now, "yyyyMMdd")
        cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.Int))
        cd.Parameters.Item("@maingroup").Value = maingroup
        cd.Parameters.Add(New SqlParameter("@text", SqlDbType.Int))
        cd.Parameters.Item("@text").Value = text
        cd.Parameters.Add(New SqlParameter("@picture", SqlDbType.Int))
        cd.Parameters.Item("@picture").Value = picture
        cd.ExecuteNonQuery()
        dbNTB.Close()
    End Function

    Public Function BitCommaText(ByVal value As Integer) As String
        Dim no1, no2 As Integer
        Dim res As String
        no2 = value
        While (no2 > 0)
            no1 = 1
            While no1 <= no2
                no1 = no1 * 2
            End While
            If res = "" Then res = CStr(no1 / 2) Else res = res + "," + CStr(no1 / 2)
            no2 = no2 - no1 / 2
        End While
        BitCommaText = res
    End Function

    Public Sub AddHit(ByVal isUnique As Boolean, ByVal sitecode As String)
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("ConString"))
        Dim cd As New SqlCommand("_UpdateStats", dbNTB)
        cd.CommandType = CommandType.StoredProcedure

        cd.Parameters.Add("@Unique", SqlDbType.Int)
        If isUnique Then
            cd.Parameters("@Unique").Value = 1
        Else
            cd.Parameters("@Unique").Value = 0
        End If
        cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
        cd.Parameters.Item("@sitecode").Value = sitecode
        dbNTB.Open()
        cd.ExecuteNonQuery()
        dbNTB.Close()
    End Sub

    Public Sub AddUserHit(ByVal username As String, ByVal login As Boolean, ByVal articleView As Boolean, ByVal sitecode As String)
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("ConString"))
        Dim cd As New SqlCommand("_UpdateUserStats", dbNTB)
        cd.CommandType = CommandType.StoredProcedure

        cd.Parameters.Add(New SqlParameter("@username", SqlDbType.VarChar, 20))
        cd.Parameters.Item("@username").Value = username

        cd.Parameters.Add("@login", SqlDbType.Int)
        If login Then
            cd.Parameters("@login").Value = 1
        Else
            cd.Parameters("@login").Value = 0
        End If

        cd.Parameters.Add("@articleView", SqlDbType.Int)
        If articleView Then
            cd.Parameters("@articleView").Value = 1
        Else
            cd.Parameters("@articleView").Value = 0
        End If

        cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
        cd.Parameters.Item("@sitecode").Value = sitecode

        dbNTB.Open()
        cd.ExecuteNonQuery()
        dbNTB.Close()
    End Sub

    Public Function MakeGifArticle(ByVal refid As Integer, ByVal articlexml As String, ByVal xsltfilename As String, ByVal temppath As String) As String
        Dim xml As New System.Xml.XmlDocument
        Dim res As New System.Xml.XmlUrlResolver
        Dim xsl As New System.Xml.Xsl.XslTransform
        Dim sb As New System.Text.StringBuilder
        Dim sw As New System.IO.StringWriter(sb)
        Dim xmlresolver As System.Xml.XmlResolver

        Dim doc As String
        xml.LoadXml(articlexml)
        xsl.Load(xsltfilename)
        xsl.Transform(xml.CreateNavigator(), Nothing, sw, res)
        doc = sb.ToString()

        Dim i As Integer
        Dim bm, bmout As Bitmap
        Dim gr As Graphics
        Dim w As Integer = CInt(ConfigurationSettings.AppSettings("gifarticlewidth"))
        Dim wstr As Integer
        bm = New Bitmap(w, 4000)
        gr = Graphics.FromImage(bm)
        Dim f As Font
        Dim x, y, wcount As Integer
        Dim br As New SolidBrush(Color.DarkBlue)
        Dim s, oldtag, tag, fn, sout As String
        Dim lines, fontdata, words As Array
        Dim fstyle As FontStyle
        Dim fsize As Single

        x = 0
        y = 0
        gr.Clear(Color.White)
        lines = Split(doc, vbNewLine)
        For i = 0 To lines.Length - 1
            s = lines(i)
            If s <> "" Then
                If s.Substring(0, 1) = "@" Then tag = s.Substring(1, s.IndexOf(":") - 1)
                s = s.Substring(s.IndexOf(":") + 1)
                If tag <> oldtag Then ' bytt font
                    fontdata = Split(tag, ";")
                    Select Case fontdata(1).ToLower
                        Case "regular", "" : fstyle = FontStyle.Regular
                        Case "bold" : fstyle = FontStyle.Bold
                        Case "italic" : fstyle = FontStyle.Italic
                        Case "bold-italic" : fstyle = FontStyle.Italic + FontStyle.Bold
                    End Select
                    fn = fontdata(0).ToString
                    fsize = fontdata(2)
                    f = Nothing
                    f = New Font(fn, fsize, fstyle)
                End If
                wcount = 0
                wstr = 0
                sout = ""
                words = Split(s)
                While wcount < words.Length ' skriv et avsnitt (en tag)
                    While (wstr < w) And (wcount < words.Length) ' skriv en linje
                        wstr = gr.MeasureString(sout & words(wcount), f).Width
                        If wstr < w Then
                            sout += words(wcount) & " "
                            wcount += 1
                        End If
                    End While
                    gr.DrawString(sout, f, br, x, y)
                    wstr = 0
                    sout = ""
                    y += gr.MeasureString("�j", f).Height
                End While
                oldtag = tag
            End If
        Next
        gr.Flush()
        bmout = New Bitmap(w + 10, y + 10)
        Dim rect As System.Drawing.Rectangle
        rect.X = 0
        rect.Y = 0
        rect.Width = w
        rect.Height = y
        bmout = bm.Clone(rect, Imaging.PixelFormat.Undefined)
        bmout.Save(temppath & "\ART" & CStr(refid) & ".gif", System.Drawing.Imaging.ImageFormat.Gif)
        bm.Dispose()
        bmout.Dispose()
        gr.Dispose()
        MakeGifArticle = "temp\ART" & CStr(refid) & ".gif"
    End Function

    Public Function ToHTML(ByVal s As String) As String
        Dim ts As String
        ts = s
        ts = ts.Replace(vbNewLine, "<br>")
        ts = ts.Replace(Chr(10), "<br>")
        ToHTML = ts
    End Function

    Public Function GetUserData(ByVal userid As String, ByVal field As String) As String
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand("select " & field & " from users where UserId = '" & userid & "'", dbNTB)
        Dim dr As SqlDataReader

        dbNTB.Open()
        GetUserData = ""
        dr = cd.ExecuteReader
        If dr.Read Then GetUserData = dr(field).ToString()
        dr.Close()
        dbNTB.Close()
    End Function

    Public Function GetSectionData(ByVal section As String, ByVal field As String) As String
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand("select " & field & " from sections where name = '" & section & "'", dbNTB)
        Dim dr As SqlDataReader

        dbNTB.Open()
        GetSectionData = ""
        dr = cd.ExecuteReader
        If dr.Read Then GetSectionData = dr(field).ToString()
        dr.Close()
        dbNTB.Close()
    End Function

    Public Function GetMainGroupOptions(ByVal userid As String) As String
        Dim defarr As Array = Split(ConfigurationSettings.AppSettings("NyhetstjenesteMaingroups"), ",")
        Dim arr As Array = Split(GetUserData(userid, "MainGroups"), ",")
        Dim i, all As Integer

        GetMainGroupOptions = ""
        all = 0
        If arr(0) = "-1" Then
            For i = 0 To defarr.Length - 1
                GetMainGroupOptions += "<option value='" & defarr(i) & "'>" & _GlobalMaingroups.Item(CInt(defarr(i))) & vbNewLine
                all += defarr(i)
            Next
        Else
            For i = 0 To arr.Length - 1
                If defarr.IndexOf(defarr, arr(i)) = -1 Then arr(i) = 0
            Next
            For i = 0 To arr.Length - 1
                If arr(i) <> 0 Then
                    GetMainGroupOptions += "<option value='" & arr(i) & "'>" & _GlobalMaingroups.Item(CInt(arr(i))) & vbNewLine
                    all += arr(i)
                End If
            Next
        End If
        GetMainGroupOptions = "<option value='" & CStr(all) & "'>Alle" & vbNewLine & GetMainGroupOptions
    End Function

    Public Function ReplaceTags(ByVal s As String, Optional ByVal section As String = "") As String
        Dim str As String
        str = s
        If str.IndexOf("<%sectionpicture%>") > -1 And section <> "" Then
            If GetSectionData(section, "PictureWidth") < GetSectionData(section, "PictureHeight") Then
                str = str.Replace("<%sectionpicture%>", "<table cellpadding=2 cellspacing=0 border=0><tr><td valign=top><img src='picture.aspx?Section=" & section & "' alt='' border='0'></td><td valign='top'><font class='forside_scanpixebildetekst'><%sectionpicturetext%></font></td></tr></table>") '<img src="<%sectionpicture%>" alt="" border="0"><br>
            Else
                str = str.Replace("<%sectionpicture%>", "<table cellpadding=2 cellspacing=0 border=0><tr><td valign=top><img src='picture.aspx?Section=" & section & "' alt='' border='0'></td></tr><tr><td valign=top><font class='forside_scanpixebildetekst'><%sectionpicturetext%></font></td></tr></table>") '<img src="<%sectionpicture%>" alt="" border="0"><br>
            End If
        End If

        If str.IndexOf("<%sectionpicturetext%>") > -1 And section <> "" Then str = str.Replace("<%sectionpicturetext%>", GetSectionData(section, "PictureText"))
        If str.IndexOf("<%sectionpictureheading%>") > -1 And section <> "" Then str = str.Replace("<%sectionpictureheading%>", GetSectionData(section, "PictureHeading"))

        If str.IndexOf("<%contactlist%>") > -1 Then str = str.Replace("<%contactlist%>", GetPhoneList(phonebookSort))
        If str.IndexOf("<%tickeritems%>") > -1 Then str = str.Replace("<%tickeritems%>", GetTickerItems(ConfigurationSettings.AppSettings("TickerCount")))
        If str.IndexOf("<%direktelink%>") > -1 Then str = str.Replace("<%direktelink%>", GetDirekteLink)

        If str.IndexOf("<%ms_siste%>") > -1 Then str = str.Replace("<%ms_siste%>", GetMSSiste(30, "main"))
        If str.IndexOf("<%ms_anbefalinger%>") > -1 Then str = str.Replace("<%ms_anbefalinger%>", GetMSAnbefalinger(30, "main"))

        ReplaceTags = str
    End Function

    Public Function GetMSAnbefalinger(ByVal count As Integer, Optional ByVal target As String = "_top", Optional ByVal timestamp As Boolean = True) As String
        Dim dbForum As New SqlConnection(ConfigurationSettings.AppSettings("forumConString"))
        Dim cd As New SqlCommand
        Dim dr As SqlDataReader
        Dim tmp, ret As String

        cd.Connection = dbForum
        cd.CommandText = "msTopicItems"
        cd.CommandType = CommandType.StoredProcedure

        cd.Parameters.Add(New SqlParameter("@forumID", SqlDbType.Int))
        cd.Parameters.Item("@forumID").Value = 2

        cd.Parameters.Add(New SqlParameter("@count", SqlDbType.Int))
        cd.Parameters.Item("@count").Value = count

        cd.Parameters.Add(New SqlParameter("@topicOnly", SqlDbType.Int))
        cd.Parameters.Item("@topicOnly").Value = 1

        cd.Parameters.Add(New SqlParameter("@sortDesc", SqlDbType.Int))
        cd.Parameters.Item("@sortDesc").Value = 1

        dbForum.Open()
        dr = cd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read()
                tmp = ""

                Dim name As String = ""
                If Not dr.IsDBNull(dr.GetOrdinal("real_name")) Then
                    name = dr("real_name")
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("name")) And name = "" Then
                    name = dr("name")
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("username")) And name = "" Then
                    name = dr("username")
                End If

                If timestamp Then
                    If dr("start_date") = dr("message_date") Then
                        tmp &= "<a href=""view_forumitem.aspx?FID=2&TID=" & dr("topic_id") & """ target=""" & target & """>"
                        tmp &= Format(dr("start_date"), "dd.MM.yyyy HH:mm") & " - " & dr("Subject").Replace("""", "&quot;")
                    Else
                        tmp &= "<a href=""view_forumitem.aspx?FID=2&TID=" & dr("topic_id") & "&get=#" & dr("thread_id") & """ target=""" & target & """>"
                        tmp &= Format(dr("message_date"), "dd.MM.yyyy HH:mm") & " - Svar: " & dr("Subject").Replace("""", "&quot;")
                    End If
                Else
                    If dr("start_date") = dr("message_date") Then
                        tmp &= "<a href=""view_forumitem.aspx?FID=2&TID=" & dr("topic_id") & """ target=""" & target & """>"
                        tmp &= dr("Subject").Replace("""", "&quot;")
                    Else
                        tmp &= "<a href=""view_forumitem.aspx?FID=2&TID=" & dr("topic_id") & "&get=#" & dr("thread_id") & """ target=""" & target & """>"
                        tmp &= "Svar: " & dr("Subject").Replace("""", "&quot;")
                    End If
                End If

                tmp &= "</a><br>" & vbCrLf

                ret &= tmp
            End While
        Else
            ret = "Ingen anbefalinger"
        End If

        dr.Close()
        dbForum.Close()

        Return ret
    End Function

    Public Function GetMSSiste(ByVal count As Integer, Optional ByVal target As String = "_top", Optional ByVal timestamp As Boolean = True) As String
        Dim dbForum As New SqlConnection(ConfigurationSettings.AppSettings("forumConString"))
        Dim cd As New SqlCommand
        Dim dr As SqlDataReader
        Dim tmp, ret As String

        cd.Connection = dbForum
        cd.CommandText = "msTopicItems"
        cd.CommandType = CommandType.StoredProcedure

        cd.Parameters.Add(New SqlParameter("@forumID", SqlDbType.Int))
        cd.Parameters.Item("@forumID").Value = 1

        cd.Parameters.Add(New SqlParameter("@count", SqlDbType.Int))
        cd.Parameters.Item("@count").Value = count

        cd.Parameters.Add(New SqlParameter("@topicOnly", SqlDbType.Int))
        cd.Parameters.Item("@topicOnly").Value = 1

        cd.Parameters.Add(New SqlParameter("@sortDesc", SqlDbType.Int))
        cd.Parameters.Item("@sortDesc").Value = 1

        dbForum.Open()
        dr = cd.ExecuteReader()

        If dr.HasRows Then
            While dr.Read()
                tmp = ""

                Dim name As String = ""
                If Not dr.IsDBNull(dr.GetOrdinal("real_name")) Then
                    name = dr("real_name")
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("name")) And name = "" Then
                    name = dr("name")
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("username")) And name = "" Then
                    name = dr("username")
                End If

                If timestamp Then
                    If dr("start_date") = dr("message_date") Then
                        tmp &= "<a href=""view_forumitem.aspx?FID=1&TID=" & dr("topic_id") & """ target=""" & target & """>"
                        tmp &= Format(dr("start_date"), "dd.MM.yyyy HH:mm") & " - " & dr("Subject").Replace("""", "&quot;")
                    Else
                        tmp &= "<a href=""view_forumitem.aspx?FID=1&TID=" & dr("topic_id") & "&get=#" & dr("thread_id") & """ target=""" & target & """>"
                        tmp &= Format(dr("message_date"), "dd.MM.yyyy HH:mm") & " - Svar: " & dr("Subject").Replace("""", "&quot;")
                    End If
                Else
                    If dr("start_date") = dr("message_date") Then
                        tmp &= "<a href=""view_forumitem.aspx?FID=1&TID=" & dr("topic_id") & """ target=""" & target & """>"
                        tmp &= dr("Subject").Replace("""", "&quot;")
                    Else
                        tmp &= "<a href=""view_forumitem.aspx?FID=1&TID=" & dr("topic_id") & "&get=#" & dr("thread_id") & """ target=""" & target & """>"
                        tmp &= "Svar: " & dr("Subject").Replace("""", "&quot;")
                    End If
                End If

                tmp &= "</a><br>" & vbCrLf

                ret &= tmp
            End While
        Else
            ret = "Ingen nyheter"
        End If

        dr.Close()
        dbForum.Close()

        Return ret
    End Function


    Public Function GetPhoneList(ByVal sort As String) As String
        Dim list As String = ConfigurationSettings.AppSettings("PhoneListXML")
        Dim sheet As String = ConfigurationSettings.AppSettings("PhoneListXSLT")

        Dim ret As StringWriter = New StringWriter

        Dim trns As System.Xml.Xsl.XslTransform = New System.Xml.Xsl.XslTransform
        Dim doc As System.Xml.XmlDocument = New System.Xml.XmlDocument
        Dim args As System.Xml.Xsl.XsltArgumentList = New System.Xml.Xsl.XsltArgumentList

        Try
            trns.Load(sheet)
            doc.Load(list)

            If sort <> "" Then
                args.AddParam("sortering", "", sort)
            End If

            trns.Transform(doc, args, ret, Nothing)

        Catch ex As Exception
            Return "Finner ikke telefonlisten." & ex.Message
        End Try

        phonebookSort = ""
        Return ret.ToString
    End Function

    Public Function GetTickerItems(ByVal count As Integer)
        Dim len As Integer = ConfigurationSettings.AppSettings("TickerLineLength")
        Dim i As Integer

        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand
        Dim dr As SqlDataReader

        cd.Connection = dbNTB
        cd.CommandText = "_GetTickers"
        cd.CommandType = CommandType.StoredProcedure
        cd.Parameters.Add(New SqlParameter("@count", SqlDbType.Int))
        cd.Parameters.Item("@count").Value = count
        dbNTB.Open()
        dr = cd.ExecuteReader()

        While dr.Read()
            Dim urg As Integer = dr("urgency")
            Dim tmp As String = dr("MessageText").Replace("""", "&quot;")
            Dim name As String = "name=""ScrollText" & CStr(i) & """"
            Dim value As String = "value=""" & Format(dr("CreationDateTime"), ConfigurationSettings.AppSettings("TickerDateFormat")) & " - "

            'Linebreaks

            Dim pos = -1
            If tmp.IndexOf(": ") > -1 Then
                pos = tmp.IndexOf(": ") + 2
            ElseIf tmp.IndexOf(") ") > -1 Then
                pos = tmp.IndexOf(") ") + 2
            End If

            If pos > -1 Then
                tmp = tmp.Insert(pos, "\n")
                pos += 2
            Else
                value += "\n"
            End If

            While pos < tmp.Length

                pos += len
                If pos < tmp.Length Then
                    While tmp.Chars(pos) <> " " And pos > 0
                        pos -= 1
                    End While

                    tmp = tmp.Insert(pos + 1, "\n")
                    pos += 3
                End If

            End While

            If urg < 4 Then
                value += tmp & ConfigurationSettings.AppSettings("TickerHASTFormatString")
            Else
                value += tmp & ConfigurationSettings.AppSettings("TickerFormatString")
            End If

            GetTickerItems += "<param " & name & " " & value & """>" & vbCrLf

            i += 1
        End While

        dr.Close()
        dbNTB.Close()
    End Function

    Public Function GetDirekteLink(Optional ByVal onlyurl As Boolean = False) As String
        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim cd As New SqlCommand("select URL, Linkname, Target from MENUDEF where SiteCode = 'SYS' and MenuId = " & ConfigurationSettings.AppSettings("DirekteLinkMenuId") & " and Active = 1", dbNTB)
        Dim dr As SqlDataReader

        dbNTB.Open()
        dr = cd.ExecuteReader(CommandBehavior.CloseConnection)
        If dr.Read Then
            If onlyurl Then
                GetDirekteLink = dr("URL")
            Else
                GetDirekteLink = "<a href='" & dr("URL") & "' target='" & dr("Target") & "'>" & dr("LinkName") & "</a>"
            End If
        End If
        dr.Close()
    End Function

    Public Function GetSiteCode() As String
        GetSiteCode = ConfigurationSettings.AppSettings("SiteCode")
    End Function

    Public Function CheckUpdateDailyPicture(ByVal path As String)
        Dim archivepath As String = path + "arkiv\" + Format(Date.Now, "yyyy") + "\" + Format(Date.Now, "MM") + "\" + Format(Date.Now, "dd") + "\"
        Dim filename As String

        Try
            filename = System.IO.Directory.GetFiles(path, "*.jpg")(0)
        Catch ex As Exception
        End Try

        Dim textfilename As String = path + "dagens.txt"

        Dim pictext As String

        If File.Exists(filename) Then
            If File.Exists(textfilename) Then
                Dim stream As StreamReader = New StreamReader(textfilename, System.Text.Encoding.GetEncoding("iso-8859-1"))
                pictext = stream.ReadToEnd
                stream.Close()
            Else
                'Get text from iptc tag
                Dim iptc As New IPTCInfo.Reader
                iptc.FromImage(filename)
                pictext = iptc.Caption
                iptc = Nothing
            End If

            If SaveDBImage(filename, "FRONTPAGE", pictext) Then
                Try
                    Directory.CreateDirectory(archivepath)
                Catch
                End Try
                Try
                    File.Delete(archivepath + "dagens.jpg")
                Catch ex As Exception
                End Try
                Try
                    File.Delete(archivepath + "dagens.txt")
                Catch ex As Exception
                End Try
                Try
                    File.Move(filename, archivepath + "dagens.jpg")
                    File.Move(textfilename, archivepath + "dagens.txt")
                Catch ex As Exception
                End Try
            End If
        End If

    End Function

    Public Function SaveDBImage(ByVal filename As String, ByVal section As String, Optional ByVal picturetext As String = "", Optional ByVal sitecode As String = "WWW") As Boolean
        Dim Image() As Byte
        Dim stream As System.IO.MemoryStream
        Dim bytecount, h, w As Integer
        Dim sql As String

        Dim bmp As Drawing.Image = ResizeImage(filename)
        h = bmp.Height
        w = bmp.Width

        ReDim Image(h * w * (bmp.GetPixelFormatSize(bmp.PixelFormat) / 8))
        stream = New System.IO.MemoryStream(Image)
        bmp.Save(stream, Drawing.Imaging.ImageFormat.Jpeg)
        stream.Close()
        stream = Nothing
        bmp = Nothing

        Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        If picturetext <> "" Then
            sql = "Update Sections set PictureText = @pictext, Picture = @picture, PictureWidth = @w, PictureHeight = @h where SiteCode = '" & sitecode & "' and Name = '" & section & "'"
        Else
            sql = "Update Sections set Picture = @picture, PictureWidth = @w, PictureHeight = @h where SiteCode = '" & sitecode & "' and Name = '" & section & "'"
        End If
        Dim cd As New SqlCommand(sql, dbNTB)
        If picturetext <> "" Then
            cd.Parameters.Add(New SqlParameter("@pictext", SqlDbType.Text))
            cd.Parameters.Item("@pictext").Value = picturetext
        End If
        cd.Parameters.Add(New SqlParameter("@picture", SqlDbType.Image))
        cd.Parameters("@picture").Value = Image
        cd.Parameters.Add(New SqlParameter("@w", SqlDbType.Int))
        cd.Parameters.Item("@w").Value = w
        cd.Parameters.Add(New SqlParameter("@h", SqlDbType.Int))
        cd.Parameters.Item("@h").Value = h

        dbNTB.Open()
        SaveDBImage = cd.ExecuteNonQuery()
        dbNTB.Close()
        Image = Nothing
    End Function

    Function ResizeImage(ByVal imgPath As String) As Drawing.Image

        ' select the format of the image to write according to the current extension
        Dim img As System.Drawing.Image = System.Drawing.Image.FromFile(imgPath)

        ' if either the specified height or width are 0, calculate it to maintain 
        ' the same ratio of the original image
        Dim mwidth As Integer
        Dim mheight As Integer

        Dim width As Integer
        Dim height As Integer

        If img.Width >= img.Height Then

            'Breddebilde
            mwidth = ConfigurationSettings.AppSettings("SectionPictureMaxWidthB")
            mheight = ConfigurationSettings.AppSettings("SectionPictureMaxHeightB")

            'Scale height first to make room for text
            If img.Height > mheight Then
                width = img.Width / (img.Height / mheight)
                height = mheight
            End If

            'If picture still is too wide, then scale width
            If width > mwidth Then
                height = height / (width / mwidth)
                width = mwidth
            End If


        Else
            'H�ydebilde
            mwidth = ConfigurationSettings.AppSettings("SectionPictureMaxWidthH")
            mheight = ConfigurationSettings.AppSettings("SectionPictureMaxHeightH")

            'Scale width first to make room for text
            If img.Width > mwidth Then
                height = img.Height / (img.Width / mwidth)
                width = mwidth
            End If

            'If picture still is too high, then scale height
            If height > mheight Then
                width = width / (height / mheight)
                height = mheight
            End If

        End If

        ' Create a new empty bitmap with the specified size
        Dim bmp As New System.Drawing.Bitmap(width, height)

        ' retrieve a canvas object that allows to draw on the empty bitmap
        Dim g As System.Drawing.Graphics = System.Drawing.Graphics.FromImage( _
            DirectCast(bmp, System.Drawing.Image))

        ' copy the original image on the canvas, and thus on the new bitmap,
        '  with the new size
        g.DrawImage(img, 0, 0, width, height)
        ' close the original image
        img.Dispose()

        ' save the new image with the proper format
        Return bmp
    End Function

End Module





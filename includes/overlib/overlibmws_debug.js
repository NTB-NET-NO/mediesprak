/*
 overlibmws_debug.js plug-in module - Copyright Foteos Macrides 2003-2004
   For support of the OLshowProperties() debugging function.
   Initial: July 26, 2003 - Last Revised: March 20, 2003
 See the Change History and Command Reference for overlibmws via:

	http://www.macridesweb.com/oltest/

 License agreement for the standard overLIB applies.  Access license via:
	http://www.bosrup.com/web/overlib/license.html
*/

// PRE-INIT
OLloaded=0;
var OLzIndex;
registerCommands('allowdebug');

/////////
// DEFAULT CONFIGURATION
if(typeof ol_allowdebug=='undefined')var ol_allowdebug="";
// END CONFIGURATION
/////////

// INIT
var o3_allowdebug="";

// For setting runtime variables to default values.
function setDebugVar(){
o3_allowdebug=ol_allowdebug;
}

// For commandline parser.
function parseDebugExtras(pf,i,ar){
var k=i;
if(ar[k]==ALLOWDEBUG){
if(k<(ar.length-1)&&typeof ar[k+1]=='string')eval(pf+"allowdebug='"+escSglQuote(ar[++k])+"'");
return k;}
return -1;
}

/////////
// DEBUG SUPPORT FUNCTIONS
/////////
function OLshowProperties(){
var args=OLshowProperties.arguments,sho,shoObj,vis,lvl=0,istrt=0,theDiv='showProps',txt='';
var fac='Verdana,Arial,Helvetica',siz=(OLns4?'1':'67%');
var fon='><font color="#000000" face="'+fac+'" size="'+siz;
var stl=' style="font-family:'+fac+';font-size:'+siz+';';
var sty=stl+'color:#000000;';
var clo=(OLns4?'</font>':'');
if(args.length==0)return;
if(args.length%2&&typeof args[0]=='string'){
istrt=1;
theDiv=args[0];}
if(OLns4){
sho=o3_frame.document.layers[theDiv];
if((typeof sho=='undefined'||!sho)&&document.classes)
sho=o3_frame.document.layers[theDiv]=new Layer(1024,o3_frame);
if(typeof sho=='undefined'||!sho)return;
shoObj=sho;
lvl=getLayerLevel(theDiv);
txt+='<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>';
}else{
lvl=getLayerLevel(theDiv);
sho=(OLie4?o3_frame.document.all[theDiv]:o3_frame.document.getElementById(theDiv));
if(sho==null){
var body=(OLie4?o3_frame.document.all.tags('body')[0]:
o3_frame.document.getElementsByTagName('body')[0]);
sho=o3_frame.document.createElement("div");
body.appendChild(sho);
sho.id=theDiv;
sho.style.position='absolute';}
shoObj=sho.style;}
if(typeof sho.position=='undefined'){
sho.position=new Pagelocation(10+lvl*20,10,1);
if(typeof OLzIndex=='undefined')OLzIndex=getDivZindex();
shoObj.zIndex=OLzIndex+1+lvl;
if(!OLns4)with(sho.style){backgroundColor='#ffffcc';padding='5px';border='1px #000000 solid';}}
txt+='<table cellpadding="5" border="0" cellspacing="0"'+(OLns4?'bgcolor="#ffffcc"':'')+'>'
+'<tr><td><strong><a href="javascript:moveToBack(\''+theDiv+'\');" title="Move to back"'
+(OLns4?fon:stl)+'">'+theDiv+clo
+'</a></strong></td><td align="right"><strong><a href="javascript:closeLayer(\''+theDiv
+'\');" title="Close Layer"'+(OLns4?fon:stl
+'background-color:#CCCCCC;border:2px #333369 outset;padding:2px;')+'">X'+clo
+'</a></strong></td></tr><tr><td'+(OLns4?fon:sty)+'">'+'<strong><em>Item</em></strong>'
+clo+'</td><td'+(OLns4?fon:sty)+'">'+'<strong><em>Value</em></strong>'+clo+'</td></tr>';
for(var i=istrt;i<args.length-1;i++)
txt+='<tr><td align="right"'+(OLns4?fon:sty)+'">'+'<strong>'+args[i]+':&nbsp;</strong>'
+clo+'</td><td'+(OLns4?fon:sty)+'">'+args[++i]+clo+'</td></tr>';
txt+='</table>'+(OLns4?'</td></tr></table>':'');
if(OLns4){
sho.document.open();
sho.document.write(txt);
sho.document.close();
}else{
if(OLie4&&OLieM)sho.innerHTML='';
sho.innerHTML=txt;}
showAllVisibleLayers();
}

function getLayerLevel(lyr){
var i=0;
if(typeof document.popups=='undefined'){
document.popups=new Array(lyr);
}else{
var l=document.popups;
for(var i=0;i<l.length;i++)if(lyr==l[i])break;
if(i==l.length)l[l.length++]=lyr;}
return i;
}

function getDivZindex(id){
var obj;
if(id==''||id==null)id='overDiv';
obj=(OLns4?o3_frame.document.layers[id]:(document.all?o3_frame.document.all[id]:
 o3_frame.document.getElementById(id)));
var obj=(OLns4?obj:obj.style);
return obj.zIndex;
}

function setCanShowParm(debugID){
if(typeof debugID!='string')return;
var lyr,pLyr=debugID.replace(/[ ]/ig,'').split(',');
for(var i=0;i<pLyr.length;i++){
lyr=OLns4?o3_frame.document.layers[pLyr[i]]:OLie4?o3_frame.document.all[pLyr[i]]:
 OLns6?o3_frame.document.getElementById(pLyr[i]):null;
if(lyr!=null&&typeof lyr.position!='undefined')lyr.position.canShow=1;}
}

function Pagelocation(x,y,canShow){
this.x=x;
this.y=y;
this.canShow=(canShow==null)?0:canShow;
}

function showAllVisibleLayers(){
var lyr,lyrObj,l=document.popups;
for(var i=0;i<l.length;i++){
lyr=(OLns4?o3_frame.document.layers[l[i]]:(document.all?o3_frame.document.all[l[i]]:
o3_frame.document.getElementById(l[i])));
lyrObj=(OLns4?lyr:lyr.style);
if(lyr.position.canShow){
positionLayer(lyrObj,lyr.position.x,lyr.position.y);
lyrObj.visibility='visible';}}
}

function positionLayer(Obj,x,y){ // Obj is obj.style for IE/NS6+ but obj for NS4
Obj.left=x+(OLie4?eval(docRoot+'.scrollLeft'):window.pageXOffset)+(OLns4?0:'px');
Obj.top=y+(OLie4?eval(docRoot+'.scrollTop'):window.pageYOffset)+(OLns4?0:'px');
}

function closeLayer(lyrID){
var lyr;
lyr=(OLns4?o3_frame.document.layers[lyrID]:(document.all?o3_frame.document.all[lyrID]:
 o3_frame.document.getElementById(lyrID)));
lyr.position.canShow=0;
lyr=(OLns4?lyr:lyr.style);
lyr.visibility='hidden';
}

function moveToBack(layer){
var l=document.popups,lyr,obj,i,x=10,dx=20,z=OLzIndex+1;
if(l.length==1)return;
lyr=(OLns4?o3_frame.document.layers[layer]:(document.all?o3_frame.document.all[layer]:
o3_frame.document.getElementById(layer)));
lyr.position.x=x;
obj=(OLns4?lyr:lyr.style);
obj.zIndex=z;
for(i=0;i<l.length;i++){
if(layer==l[i])continue;
lyr=(OLns4?o3_frame.document.layers[l[i]]:(document.all?o3_frame.document.all[l[i]]:
 o3_frame.document.getElementById(l[i])));
if(lyr.position.canShow==0)continue;
obj=(OLns4?lyr:lyr.style);
obj.zIndex+=1;
lyr.position.x+=dx;}
showAllVisibleLayers();
}

////////
// PLUGIN REGISTRATIONS
////////
registerRunTimeFunction(setDebugVar);
registerCmdLineFunction(parseDebugExtras);

OLdebugPI=1;
OLloaded=1;

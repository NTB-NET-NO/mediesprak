/*
 overlibmws_iframe.js plug-in module - Copyright Foteos Macrides 2003-2004
   Masks system controls to prevent obscuring of popops for IE v5.5 or higher.
   Initial: October 19, 2003 - Last Revised: March 21, 2004
 See the Change History and Command Reference for overlibmws via:

	http://www.macridesweb.com/oltest/

 License agreement for the standard overLIB applies.  Access license via:
	http://www.bosrup.com/web/overlib/license.html
*/

// PRE-INIT
OLloaded=0;

// INIT
var OLifShim=null,OLifShimShadow=null,OLifShimOvertwo=null,OLifShimSrc='';

////////
// IFRAME SHIM SUPPORT FUNCTIONS
////////
function initIframeRef(){
if(!OLie55)return;
OLifShimSrc=(document.URL.indexOf('https://')==0)?'/blank.html':'javascript:void(0)';
var theObj;
if((OLovertwoPI)&&over2&&over==over2){
theObj=o3_frame.document.all['overIframeOvertwo'];
if(theObj=='undefined'||!theObj||OLifShimOvertwo!=theObj){
OLifShimOvertwo=null;getIfShimOvertwoRef();}return;}
theObj=o3_frame.document.all['overIframe'];
if(theObj=='undefined'||!theObj||OLifShim!=theObj){
OLifShim=null;getIfShimRef();}
if((OLshadowPI)&&o3_shadow){
theObj=o3_frame.document.all['overIframeShadow'];
if(theObj=='undefined'||!theObj||OLifShimShadow!=theObj){
OLifShimShadow=null;getIfShimShadowRef();}}
//alert('2');
}

function setIfShimRef(obj,i,s,z){
with(obj){id=i;src=s;}
with(obj.style){position='absolute';top=0;left=0;width=1;height=1;visibility='hidden';
zIndex=over.style.zIndex-z;filter='Alpha(style=0,opacity=0)';}
}

function getIfShimRef(){
if(OLifShim||!OLie55)return;
var body=o3_frame.document.all.tags('body')[0];
OLifShim=o3_frame.document.createElement('iframe');
if(typeof OLifShim.style.filter!='string'){if(typeof OLifShim!='undefined')
OLifShim.style.display='none';OLifShim=null;OLie55=false;return;}
body.insertBefore(OLifShim,body.firstChild);
setIfShimRef(OLifShim,'overIframe',OLifShimSrc,2);
}

function getIfShimShadowRef(){
if(OLifShimShadow||!OLie55)return;
var body=o3_frame.document.all.tags('body')[0];
OLifShimShadow=o3_frame.document.createElement('iframe');
if(typeof OLifShimShadow.style.filter!='string'){if(typeof OLifShimShadow!='undefined')
OLifShimShadow.style.display='none';OLifShimShadow=null;OLie55=false;return;}
body.insertBefore(OLifShimShadow,body.firstChild);
setIfShimRef(OLifShimShadow,'overIframeShadow',OLifShimSrc,3);
}

function getIfShimOvertwoRef(){
if(OLifShimOvertwo||!OLie55)return;
var body=o3_frame.document.all.tags('body')[0];
OLifShimOvertwo=o3_frame.document.createElement('iframe');
if(typeof OLifShimOvertwo.style.filter!='string'){if(typeof OLifShimOvertwo!='undefined')
OLifShimOvertwo.style.display='none';OLifShimOvertwo=null;OLie55=false;return;}
body.insertBefore(OLifShimOvertwo,body.firstChild);
setIfShimRef(OLifShimOvertwo,'overIframeOvertwo',OLifShimSrc,1);
}

function setDispIfShim(obj,w,h){
with(obj.style){width=w+'px';height=h+'px';clip='rect(0px '+w+'px '+h+'px 0px)';}
obj.filters.alpha.enabled=true;
}

function dispIfShim(){
if(!OLie55)return;
var wd=over.offsetWidth,ht=over.offsetHeight;
if((OLovertwoPI)&&over2&&over==over2){
if(!OLifShimOvertwo)return;
setDispIfShim(OLifShimOvertwo,wd,ht);return;}
if(!OLifShim)return;
setDispIfShim(OLifShim,wd,ht);
if((!OLshadowPI)||!o3_shadow||!OLifShimShadow)return;
setDispIfShim(OLifShimShadow,wd,ht);
}

function showIfShim(){
if(OLifShim){OLifShim.style.visibility="visible";
if((OLshadowPI)&&o3_shadow&&OLifShimShadow)OLifShimShadow.style.visibility="visible";}
}

function hideIfShim(){
if(OLifShim)OLifShim.style.visibility="hidden";
if((OLshadowPI)&&o3_shadow&&OLifShimShadow)OLifShimShadow.style.visibility="hidden";
}

function repositionIfShim(X,Y){
if(OLie55){if((OLovertwoPI)&&over2&&over==over2){
if(OLifShimOvertwo)repositionTo(OLifShimOvertwo,X,Y);}
else{if(OLifShim){repositionTo(OLifShim,X,Y);if((OLshadowPI)&&o3_shadow&&OLifShimShadow)
repositionTo(OLifShimShadow,X+o3_shadowx,Y+o3_shadowy);}}}
}

OLiframePI=1;
OLloaded=1;
